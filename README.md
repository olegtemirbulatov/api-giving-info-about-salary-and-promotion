# API giving info about salary and promotion

This is a REST API service written via FastAPI giving private information about user's own salary and his/her next promotion date. This information is usually confidential, so user needs to get a special token before getting access to these data.

## Getting started

### 1.

To use the service clone this repository. 

### 2.

Then create a random secret key via a command:
```
$ openssl rand -hex 32
```
If there is no openssl installed locally it's possible to use the service https://www.cryptool.org/en/cto/openssl.

Then copy generated key and assign it to the variable **SECRET_KEY** in **main.py**:

`SECRET_KEY = "..."`

This key will be used to sign the JWT tokens.

## Run main.py

After downloading and generating a secret key run the file using command:

```
$ poetry run python -m uvicorn main:app
```

## Using the service

### 1.

To interact with the service go to http://127.0.0.1:8000/docs in your browser.

### 2.

To get access to the API's functional authorize clicking the button **Authorize** located in the upper right corner. At this example use **johndoe** as an username and **secret** as a password.


### 3.

To get a secret token providing a private info authorize with **/token**. Then copy a new valid token from the field **"access_token"** (response body).

To get a private information go to **/users/me/items**, put copied token into the **token** parameter. Run it. Needed info will be displayed in the response body.

